const express = require('express');
const router = express.Router();
const utils = require('../modules/utils');
require('dotenv').config();

// Homepage
router.get('/', function(req, res) {
  res.render('index', {title: 'Home', error: req.query.error});
});

// Private Area
router.get('/private-area', function(req, res) {
  let cookies = utils.parseCookies(req);
  if (cookies.token) {
    // If token exists access private area
    res.render('private-area',
        {title: 'Area Riservata', user: req.session.user});
  } else {
    // Redirect to SSO login page
    res.redirect(`${process.env.SSO}/?callbackUrl=${req.host}`);
  }
});

// Login route
router.get('/login', function(req, res) {
  let token = req.query.token;
  if (token) {
    // Check if token is a valid one
    utils.validateToken(token, function(data) {
      if (data) {
        // If token is valid, set cookie and session, then redirect to protected area
        req.session.user = data.user;
        res.cookie('token', token,
            {maxAge: process.env.TOKEN_EXPIRE * 1000, httpOnly: true});
        res.redirect('/private-area');
      } else {
        // Invalid token: request login
        res.redirect(`${process.env.SSO}?callbackUrl=${req.host}`);
      }
    });
  } else {
    // No token: request Login
    res.redirect(`${process.env.SSO}?callbackUrl=${req.host}`);
  }
});

// Logout
router.get('/logout', function(req, res) {
  // Clear all cookies
  res.clearCookie('token');
  res.redirect(`${process.env.SSO}/logout?callbackUrl=${req.host}`);
});

module.exports = router;
