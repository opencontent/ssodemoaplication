# Demo Authentication Application

This is a simple Application for testing SSO Authentication Server.

## Description

By running this application it will be possible to interact with the SSO authentication server.

A web interface is available in which two sections are presented: the homepage and a protected area.
By accessing the protected area if you do not have an access token, the user will be redirected 
to the authentication server, otherwise he will be able to access the reserved area where some 
information regarding the registered user profile will be displayed.

