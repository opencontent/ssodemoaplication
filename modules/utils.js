const jwtToken = require('jsonwebtoken');
const fs = require('fs');
const cert = fs.readFileSync('./certs/cert.pem');  // get public key

/**
 * Parses request cookies
 * @param request {Object}: request object
 * @return dictionary of request cookies
 */
function parseCookies(request) {
  let cookiesList = {},
      cookies = request.headers.cookie;
  cookies && cookies.split(';').forEach(function(cookie) {
    let parts = cookie.split('=');
    cookiesList[parts.shift().trim()] = decodeURI(parts.join('='));
  });
  return cookiesList;
}

/**
 * Checks if given token in a valid one
 * @param token: token to validate
 * @param callback: function where to send results
 */
function validateToken(token, callback) {
  jwtToken.verify(token, cert, {algorithm: 'RS256'}, function(err, decoded) {
    if (decoded && decoded.user) {
      callback(decoded);
    } else {
      callback(null);
    }
  });
}

module.exports = {
  parseCookies: parseCookies,
  validateToken: validateToken,
};
